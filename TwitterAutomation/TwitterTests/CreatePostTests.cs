﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterAutomation;

namespace TwitterTests
{
    [TestClass] // needs to be public !!!
    public class CreatePostTests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void CanCreateBasicPost()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("QA_Selenium").WithPassword("Testing4Life").Login();

            NewPostPage.GoTo();
            NewPostPage.CreatePost("Still testing... #Selenium #AutomatedTesting").Publish();
            
            var msg = NewPostPage.CheckMessage();
            Assert.AreEqual(msg, "Your Tweet was posted!", "Message text does not indicate successfully created new post!");
        }

        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }
    }
}
