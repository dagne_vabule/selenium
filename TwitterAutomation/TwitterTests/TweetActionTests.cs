﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterAutomation;

namespace TwitterTests
{
    [TestClass]
    public class TweetActionTests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        // REQUIRES THAT THE PARTICULAR TWEET ISN'T ALREADY RETWEETED !!!
        [TestMethod]
        public void CanRetweetAPost()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("QA_Selenium").WithPassword("Testing4Life").Login();

            UserTimelinePage.GoTo().User("karsumholikje").OpenTimeline();
            UserTimelinePage.FindLatestTweet().Retweet();

            // TO DO: Assert - check if tweet was retweeted
            Assert.IsTrue(UserTimelinePage.FindLatestTweet().IsUndoRetweetShown(), "The latest tweet wasn't retweeted!");
        }

        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }
    }
}
