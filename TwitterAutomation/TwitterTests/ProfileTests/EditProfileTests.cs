﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterAutomation.Pages;

namespace TwitterTests.ProfileTests
{
    [TestClass]
    public class EditProfileTests : TwitterTest
    {
        // <ideas>
        // --- Start actions
        // Can open profile info editor

        // --- Edit actions
        // Can change name
        // Can choose theme color
        // Can change header photo
        // Can add birthday

        // --- Finish actions
        // Can cancel changes
        // Can save changes
        // </ideas>

        [TestMethod]
        public void CanChangeThemeColor()
        {
            // <full test idea>
            // Can open profile info editor
            // Can choose theme color
            // Can save changes
            // </full test idea>


            // Go to profile, open profile editor
            ProfilePage.GoTo();
            ProfilePage.OpenEditor();

            // Select the field for Theme color, select random color from suggested color list, store the color code
            EditProfilePage.Enter(EditField.ThemeColor).Change().Color();
            EditProfilePage.StoreColor();

            // Click on "Save Changes"
            EditProfilePage.Save();

            // Check for the color used in header
            ProfilePage.GoTo();
            Assert.AreEqual(EditProfilePage.PickedColor, ProfilePage.ThemeColor, "Theme color did not change!");
        }


    }
}
