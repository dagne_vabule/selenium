﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwitterAutomation;

namespace TwitterTests
{
    [TestClass] // needs to be public !!!
    public class LoginTests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void UserCanLogin()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("QA_Selenium").WithPassword("Testing4Life").Login();

            Assert.IsTrue(MainPage.IsAt, "Failed to login.");
        }

        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }
    }
}
