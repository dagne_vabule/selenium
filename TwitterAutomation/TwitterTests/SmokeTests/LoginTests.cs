﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwitterAutomation;

namespace TwitterTests
{
    [TestClass] // needs to be public !!!
    public class LoginTests : TwitterTest
    {
        [TestMethod]
        public void UserCanLogin()
        {
            Assert.IsTrue(MainPage.IsAt, "Failed to login.");
        }
    }
}
