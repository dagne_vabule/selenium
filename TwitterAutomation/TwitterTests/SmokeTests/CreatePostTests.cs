﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterAutomation;

namespace TwitterTests
{
    [TestClass] // needs to be public !!!
    public class CreatePostTests : TwitterTest
    {
        [TestMethod]
        public void CanCreateBasicPost()
        {
            NewPostPage.GoTo();
            NewPostPage.CreatePost("Still testing... @ " + System.DateTime.Now.ToString()).Publish();
            
            var msg = NewPostPage.CheckMessage();
            Assert.AreEqual(msg, "Your Tweet was posted!", "Message text does not indicate successfully created new post!");
        }
    }
}
