﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterAutomation;

namespace TwitterTests
{
    [TestClass]
    public class TweetActionTests : TwitterTest
    {
        // REQUIRES THAT THE PARTICULAR TWEET ISN'T ALREADY RETWEETED !!!
        [TestMethod]
        public void CanRetweetAPost()
        {
            UserTimelinePage.GoTo().User("karsumholikje").OpenTimeline();
            UserTimelinePage.FindLatestTweet().Retweet();

            // TO DO: Assert - check if tweet was retweeted
            Assert.IsTrue(UserTimelinePage.FindLatestTweet().IsUndoRetweetShown(), "The latest tweet wasn't retweeted!");
        }
    }
}
