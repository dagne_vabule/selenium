﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class TweetAction
    {
        public class Reply
        {
            public static void Click(IWebElement tweet)
            {
                ActionSelector.Select(tweet, "js-actionReply");
            }
        }

        public class Retweet
        {
            public static void Click(IWebElement tweet)
            {
                ActionSelector.Select(tweet, "js-actionRetweet");
            }
        }

        public class Favorite
        {
            public static void Click(IWebElement tweet)
            {
                ActionSelector.Select(tweet, "js-actionFavorite");
            }
        }
    }

    public class ActionSelector
    {
        public static void Select(IWebElement tweet, string actionButton)
        {
            var buttons = tweet.FindElements(By.ClassName(actionButton));
            if (buttons.Count > 0)
            {
                //wait.Until(ExpectedConditions.ElementToBeClickable(buttons[0]));
                buttons[0].Click();
            }
        }
    }
}
