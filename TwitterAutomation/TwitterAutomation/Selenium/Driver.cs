﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }
        public static WebDriverWait Wait;

        public static void Initialize()
        {
            Instance = new FirefoxDriver();
            Wait = new WebDriverWait(Instance, TimeSpan.FromSeconds(20));
        }

        public static void Close()
        {
            Instance.Close();
        }

        internal static void WaitForSeconds(int sec)
        {
            Thread.Sleep(sec * 1000);
        }

        public static string BaseAddress
        {
            get { return "https://twitter.com/"; }
        }
    }
}
