﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class NewPostPage
    {
        public static void GoTo()
        {
            //var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(2));
            Thread.Sleep(2000);
            var newTweetButton = Driver.Instance.FindElement(By.Id("global-new-tweet-button"));
            newTweetButton.Click();
        }

        public static CreatePostCommand CreatePost(string text)
        {
            return new CreatePostCommand(text);
        }

        public static string CheckMessage()
        {
            Thread.Sleep(1000); // WAIT !
            var postedMessage = Driver.Instance.FindElement(By.Id("message-drawer")).FindElement(By.TagName("span"));
            return postedMessage.Text;
        }
    }

    public class CreatePostCommand
    {
        private string text { get; set; }

        public CreatePostCommand(string text)
        {
            this.text = text;
        }

        public void Publish()
        {
            Thread.Sleep(1500);

            var tweetArea = Driver.Instance.FindElement(By.Id("tweet-box-global"));
            tweetArea.SendKeys(text);

            var tweetActionBtns = Driver.Instance.FindElements(By.ClassName("tweet-action"));
            if (tweetActionBtns.Count > 0)
            {
                for (int i = 0; i < tweetActionBtns.Count; i++)
                {
                    if (tweetActionBtns[i].Displayed)
                    {
                        tweetActionBtns[i].Click();
                    }
                }
            }

            //var tweetButton = Driver.Instance.FindElement(By.LinkText("Tweet"));
            //tweetButton.Click();
        }
    }
}
