﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class LoginPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("https://twitter.com/?lang=en");
        }

        public static LoginCommand LoginAs(string userName)
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("signin-email")));
            return new LoginCommand(userName);
        }
    }

    public class LoginCommand
    {
        private readonly string userName;
        private string password;

        public LoginCommand(string userName)
        {
            this.userName = userName;
        }

        public LoginCommand WithPassword(string password)
        {
            this.password = password;
            return this;
        }

        public void Login()
        {
            var loginInput = Driver.Instance.FindElement(By.Id("signin-email"));
            loginInput.SendKeys(userName);

            var passwordInput = Driver.Instance.FindElement(By.Id("signin-password"));
            passwordInput.SendKeys(password);

            //var loginButton = Driver.Instance.FindElement(By.XPath("//button[@type='submit']/text()='Log in'"));
            var buttons = Driver.Instance.FindElements(By.TagName("button"));
            if (buttons.Count > 0)
            {
                buttons[1].Click(); // found out by trying...
            }
            
        }
        /*
         * Email / username => id="signin-email"
         * Password => id="signin-password"
         * Login button => ...

         * Element to check => TagName = span, Text = Notifications
         */
    }
}
