﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class MainPage
    {
        public static bool IsAt
        {
            get 
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(30));
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("user-dropdown-toggle"))); // button for "Profile and settings"

                var timeline = Driver.Instance.FindElement(By.Id("timeline"));
                return timeline.Displayed;
            }
        }
    }
}
