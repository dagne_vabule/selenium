﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class UserTimelinePage
    {
        public static UserTimelineCommand GoTo()
        {
            // wait until the main page is opened
            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.Id("user-dropdown-toggle"))); // button for "Profile and settings"
            
            return new UserTimelineCommand();
        }

        public static FindLatestTweetCommand FindLatestTweet()
        {
            return new FindLatestTweetCommand();
        }
    }

    public class UserTimelineCommand
    {
        private string username { get; set; }

        public UserTimelineCommand User(string username)
        {
            this.username = username;
            return this;
        }

        public void OpenTimeline()
        {
            // Search for name
            var searchBox = Driver.Instance.FindElement(By.Id("search-query"));
            searchBox.SendKeys(username);

            var searchForm = Driver.Instance.FindElement(By.Id("global-nav-search"));
            var searchBtn = searchForm.FindElements(By.TagName("button"));
            if (searchBtn.Count > 0)
                searchBtn[0].Click();

            #region Not working solutions
            // None of these are reliable - sometimes working, sometimes not...
            // - version 1
            //Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("SearchExtrasDropdown")));
            //Driver.Wait.Until(ExpectedConditions.TitleContains("karsumholikje")); // title represents the searched phrase

            // - version 2
            //Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[text()='People']")));
            #endregion

            Driver.WaitForSeconds(3); // wait for 3 sec to let link elements load

            // Find the specific profile
            var links = Driver.Instance.FindElements(By.TagName("a"));
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i].Text == "PEOPLE")
                {
                    Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(links[i]));
                    links[i].Click();
                    break;
                }
            }

            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("user-actions-follow-button"))); // If Follow button is present, that means some profiles are shown

            // Open profile - timeline
            var userProfileNames = Driver.Instance.FindElements(By.ClassName("ProfileNameTruncated-link"));
            for (int i = 0; i < userProfileNames.Count; i++)
            {
                if (userProfileNames[i].Text == "Dagne")
                {
                    Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(userProfileNames[i]));
                    userProfileNames[i].Click();
                    break;
                }
            }
        }
    }

    public class FindLatestTweetCommand
    {
        private IWebElement latestTweet { get; set; }

        public FindLatestTweetCommand()
        {
            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("tweet")));
            var tweets = Driver.Instance.FindElements(By.ClassName("tweet"));
            this.latestTweet = tweets[0];
        }

        // REQUIRES THAT THE PARTICULAR TWEET ISN'T ALREADY RETWEETED !!!
        public void Retweet()
        {
            TweetAction.Retweet.Click(this.latestTweet);
            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.Id("retweet-tweet-dialog")));

            // Seems a good approach, but - NOT WORKING !
            //Driver.Instance.SwitchTo().ActiveElement().Click(); // The Retweet button has focus by default

            var retweetBtns = Driver.Instance.FindElements(By.ClassName("retweet-action"));
            if(retweetBtns.Count > 0)
                retweetBtns[0].Click();
        }

        public bool IsUndoRetweetShown()
        {
            var undoRetweetBtns = latestTweet.FindElements(By.ClassName("ProfileTweet-actionButtonUndo"));
            if (undoRetweetBtns.Count > 1)
            {
                for (int i = 0; i < undoRetweetBtns.Count; i++)
                {
                    if (undoRetweetBtns[i].Displayed) // if "Undo Retweet" button is visible, then the tweet has been retweeted
                        return true;
                }
            }
            return false;
        }
    }
}
