﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAutomation.Pages
{
    public class EditProfilePage
    {
        public static string lastThemeColor { get; set; }

        public static EditCommand Enter(EditField editField)
        {
            string fieldId = String.Empty;
            switch (editField)
            {
                case EditField.Name:
                    fieldId = "user_name";
                    break;
                case EditField.Bio:
                    break;
                case EditField.Location:
                    break;
                case EditField.Website:
                    break;
                case EditField.ThemeColor:
                    fieldId = "js-userColorButton";
                    break;
                case EditField.Birthday:
                    break;
                default:
                    break;
            }

            Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(fieldId)));
            var field = Driver.Instance.FindElement(By.Id(fieldId));
            field.Click();

            return new EditCommand(fieldId);
        }

        public static void StoreColor()
        {
            lastThemeColor = GetThemeColor();
        }

        private static string GetThemeColor()
        {
            string colorCode = "";
            // To Do: Implement mechanism to get the color code for selected color
            return colorCode;
        }



        public static void Save()
        {
            throw new NotImplementedException();
        }

        public static string PickedColor { get; set; }
    }
    public enum EditField
    {
        Name,
        Bio,
        Location,
        Website,
        ThemeColor,
        Birthday
    }
}
