﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TwitterAutomation.Pages
{
    public class EditCommand
    {
        private string selectedFieldId;
        public EditCommand(string field)
        {
            this.selectedFieldId = field;
        }

        public EditCommand Change()
        {
            return this;
        }

        public void Color()
        {
            // Chooses random color from given palette, then selects it
            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("ColorPicker-colorList")));
            var colorPickers = Driver.Instance.FindElements(By.ClassName("ColorPicker-colorList"));

            var colors = colorPickers[0].FindElements(By.ClassName("ColorPicker-item"));

            Random r = new Random();
            var randomColorNumber = r.Next(0, colors.Count - 1);
            Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(colors[randomColorNumber]));
            colors[randomColorNumber].Click();

        }
        public void Color(string colorCode)
        {
            // Chooses color based on given colorCode, then selects it
        }
    }
}
