﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public class NewPostPage
    {
        public static void GoTo()
        {
            Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("global-new-tweet-button")));
            var newTweetButton = Driver.Instance.FindElement(By.Id("global-new-tweet-button"));
            newTweetButton.Click();
        }

        public static CreatePostCommand CreatePost(string text)
        {
            return new CreatePostCommand(text);
        }

        public static string CheckMessage()
        {
            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.Id("message-drawer")));
            var postedMessage = Driver.Instance.FindElement(By.Id("message-drawer")).FindElement(By.TagName("span"));
            return postedMessage.Text;
        }
    }

    public class CreatePostCommand
    {
        private string text { get; set; }

        public CreatePostCommand(string text)
        {
            this.text = text;
        }

        public void Publish()
        {
            Driver.Wait.Until(ExpectedConditions.ElementIsVisible(By.Id("tweet-box-global")));
            var tweetArea = Driver.Instance.FindElement(By.Id("tweet-box-global"));
            tweetArea.SendKeys(text);

            var tweetActionBtns = Driver.Instance.FindElements(By.ClassName("tweet-action"));
            if (tweetActionBtns.Count > 0)
            {
                for (int i = 0; i < tweetActionBtns.Count; i++)
                {
                    if (tweetActionBtns[i].Text == "Tweet")
                    {
                        Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(tweetActionBtns[i]));
                        tweetActionBtns[i].Click();
                    }
                }
            }
        }
    }
}
