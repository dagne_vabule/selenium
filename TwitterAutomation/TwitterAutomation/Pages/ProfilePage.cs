﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAutomation.Pages
{
    public class ProfilePage
    {
        public static void GoTo()
        {
            // --- 1
            Driver.WaitForSeconds(3);
            Driver.Wait.Until(ExpectedConditions.ElementExists(By.ClassName("DashboardProfileCard")));
            var profiles = Driver.Instance.FindElements(By.ClassName("DashboardProfileCard"));

            var links = profiles[0].FindElements(By.TagName("a"));
            foreach (var link in links)
            {
                if (link.Text.Contains("Selenium") || link.GetAttribute("href").Contains("QA_Selenium"))
                {
                    Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(link));
                    link.Click();
                    break;
                }
            }

            // --- 2
            //Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("user-dropdown-toggle")));
            //var profileButton = Driver.Instance.FindElement(By.Id("user-dropdown-toggle"));
            //profileButton.Click();

            //Driver.Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.ClassName("fullname")));

            //var names = Driver.Instance.FindElements(By.ClassName("fullname"));

            //foreach (var name in names)
            //{
            //    if (name.Text.Contains("Selenium Testing"))
            //    {
            //        Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(name));
            //        name.Click();
            //    }
            //}

            // --- 3
            //Driver.WaitForSeconds(10);

            //var links = Driver.Instance.FindElements(By.TagName("a"));
            //foreach (var l in links)
            //{
            //    if (l.GetAttribute("href") == "/QA_Selenium" && l.GetAttribute("class") == "u-textInheritColor js-nav")
            //    {
            //        Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(l));
            //        l.Click();
            //        break;
            //    }
            //}
        }

        public static void OpenEditor()
        {
            Driver.WaitForSeconds(3);

            var editButtons = Driver.Instance.FindElements(By.ClassName("UserActions-editButton"));
            foreach (var button in editButtons)
            {
                if (button.Text == "Edit profile")
                {
                    Driver.Wait.Until(ExpectedConditions.ElementToBeClickable(button));
                    button.Click();
                    break;
                }
            }
        }

        public static string ThemeColor { get; set; }
    }
}
